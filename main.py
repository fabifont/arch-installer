import os
import sys
import subprocess


W = "\033[0m"   # white
R = "\033[31m"  # red
G = "\033[32m"  # green
B = "\033[36m"  # blue

if __name__ == "__main__":
  print(f"{B}##########################################\n#{W}             ARCH INSTALLER             {B}#\n##########################################{B}\n\n")

  print(f"{G}EFI mode check{W}\n")
  if "No such file or directory" in subprocess.getoutput("ls /sys/firmware/efi/efivars"):
    sys.exit(f"{R}You are not in EFI mode{W}\n")
  print(f"{G}You are in EFI mode{W}\n")

  print(f"{B}I need some informations that are essential for the installation{W}\n")
  os.system("fdisk -l")
  disk = input(f"{B}Select the disk where you want to install Arch Linux (example /dev/sda): {W}")
  boot_size = "550M"
  boot_size = input(f"{B}Select the size for boot partition (default: {boot_size}): {W}") or boot_size
  root_size = input(f"{B}Select the size for boot partition (rest): {W}") or ""

  packages = "base base-devel btrfs-progs linux linux-firmware dhcpcd nano iwd networkmanager python"
  cpus = ["1", "2"]
  while (cpu := input(f"{B}Your cpu is: (1) Intel, (2) AMD: {W}")) not in cpus:
    cpu = input(f"{B}Your cpu is: (1) Intel, (2) AMD: {W}")
  packages = input(f"{B}Enter the packages you need separated by a space. (default: {packages}):{W}\n") or packages
  if cpu == "1" and "intel-ucode" not in packages:
    packages += " intel-ucode"
  elif cpu == "2" and "amd-ucode" not in packages:
    packages += " amd-ucode"
  zoneinfo = "Europe/Rome"
  zoneinfo = input(f"{B}Enter the zoneinfo (default: {zoneinfo}): {W}") or zoneinfo
  hostname = "arch"
  hostname = input(f"{B}Enter the hostname (default: {hostname}): {W}") or hostname

  os.system("timedatectl set-ntp true")

  keystroke = f"o\nY\nn\n\n\n{boot_size}\nEF00\nn\n\n\n{root_size}\n\nw\nY\n"
  with open("gdisk.txt", "w") as f:
    f.write(keystroke)
    f.close()

  os.system(f"gdisk {disk} < gdisk.txt")

  if "nvme" in disk:
    disk += "p"

  print(f"{B}\nSetup luks password{W}")
  os.system(f"cryptsetup luksFormat {disk}2")
  print(f"{B}\nEnter the password you just created{W}\n")
  os.system(f"cryptsetup open {disk}2 luks")

  os.system(f"mkfs.vfat -F32 -n EFI {disk}1")
  os.system("mkfs.btrfs -L ROOT /dev/mapper/luks")

  os.system("mount /dev/mapper/luks /mnt")
  os.system("btrfs sub create /mnt/@")
  os.system("btrfs sub create /mnt/@home")
  os.system("btrfs sub create /mnt/@pkg")
  os.system("btrfs sub create /mnt/@snapshots")
  os.system("btrfs sub create /mnt/@btrfs")
  os.system("umount /mnt")

  os.system("mount -o noatime,nodiratime,compress=zstd,space_cache,ssd,subvol=@ /dev/mapper/luks /mnt")
  os.system("mkdir -p /mnt/{boot,home,var/cache/pacman/pkg,.snapshots,btrfs}")
  os.system("mount -o noatime,nodiratime,compress=zstd,space_cache,ssd,subvol=@home /dev/mapper/luks /mnt/home")
  os.system("mount -o noatime,nodiratime,compress=zstd,space_cache,ssd,subvol=@pkg /dev/mapper/luks /mnt/var/cache/pacman/pkg")
  os.system("mount -o noatime,nodiratime,compress=zstd,space_cache,ssd,subvol=@snapshots /dev/mapper/luks /mnt/.snapshots")
  os.system("mount -o noatime,nodiratime,compress=zstd,space_cache,ssd,subvolid=5 /dev/mapper/luks /mnt/btrfs")

  os.system(f"mount {disk}1 /mnt/boot")
  os.system(f"pacstrap /mnt {packages}")

  os.system("genfstab -U /mnt >> /mnt/etc/fstab")
  uuid = subprocess.getoutput(f"blkid -s UUID -o value {disk}2")

  with open("/mnt/etc/locale.gen", "r+") as f:
    locale = f.read()
    locale.replace("#en_US.UTF-8 UTF-8", "en_US.UTF-8 UTF-8")
    f.seek(0)
    f.write(locale)

  with open("/mnt/etc/hosts", "a") as f:
    f.write(f"127.0.0.1       localhost\n::1             localhost\n127.0.0.1       {hostname}.localdomain        {hostname}")

  with open("/mnt/etc/mkinitcpio.conf", "r+") as f:
    mkinitcpio_lines = (f.readlines())[::-1]
    f.seek(0)
    mkinitcpio = f.read()
    for line in mkinitcpio_lines:
      if "HOOKS=" in line:
        mkinitcpio = mkinitcpio.replace(line, "HOOKS=\"base keyboard udev autodetect modconf block keymap encrypt btrfs filesystems\"\n")
        break
    f.seek(0)
    f.write(mkinitcpio)

  keystroke = f"echo 'LANG=en_US.UTF-8' > /etc/locale.conf\necho {hostname} > /etc/hostname\nlocale-gen\nln -sf /usr/share/zoneinfo/{zoneinfo} /etc/localtime\nhwclock --systohc\nmkinitcpio -p linux\nbootctl --path=/boot install\nexit\n"
  with open("chroot.txt", "w") as f:
    f.write(keystroke)

  os.system("arch-chroot /mnt < chroot.txt")

  with open("/mnt/boot/loader/loader.conf", "w") as f:
    f.write("default arch.conf")

  with open("/mnt/boot/loader/entries/arch.conf", "w") as f:
    f.write(
        f"title Arch Linux\nlinux /vmlinuz-linux\ninitrd /{'intel' if cpu == '1' else 'amd'}-ucode.img\ninitrd /initramfs-linux.img\noptions cryptdevice=UUID={uuid}:luks:allow-discards root=/dev/mapper/luks rootflags=subvol=@ rd.luks.options=discard rw")

  print(f"{B}Type \"passwd\" to set the root password. Once done type \"exit\", then \"umount -R /mnt\" and finally \"reboot\"!{W}\n")
  os.system("arch-chroot /mnt")
