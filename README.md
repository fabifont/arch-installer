# arch-installer
`arch-installer` is a simple python script that helps you with Arch Linux installation.
It will perform the installation with btrfs, luks and systemd-boot.
You can choose:
- boot partition size
- root partition size
- luks password
- root password
- packages to pacstrap
- cpu ucode
- zoneinfo
- hostname

## Run
1. `curl https://gitlab.com/api/v4/projects/28800371/repository/files/main.py/raw\?ref\=main --output main.py`
2. `python3 main.py`
